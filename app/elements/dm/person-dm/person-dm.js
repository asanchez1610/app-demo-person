import { LitElement } from 'lit-element';
import { PersonService } from './PersonService.js';

class PersonDm extends LitElement {
  get personService() {
    return new PersonService();
  }

  maskLoading(show) {
    window.dispatchEvent(
      new CustomEvent('loading-mask', {
        detail: show,
        bubbles: true,
        composed: true
      })
    );
  }

  async findPerson() {
    this.maskLoading(true);  
    const data = await this.personService.findPerson();
    this.maskLoading(false);
    return  data;
  }

  async savePerson(person) {
    this.maskLoading(true);
    const data = await this.personService.savePerson(person);
    this.maskLoading(false);
    return data;
  }

  async deletePerson(id) {
    this.maskLoading(true);
    await this.personService.deletePerson(id);
    this.maskLoading(false);
  }
  
}
customElements.define('person-dm', PersonDm);