import { BaseService } from '../../../elements/utils/BaseService.js';
export class PersonService extends BaseService {
  constructor() {
    super();
  }

  async findPerson() {
    return await fetch(
      `${this.host}/${this.endPoints.persons}`
    ).then((response) => response.json());
  }

  async savePerson(person) {
    let myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');
    let config = {
      method: person._id ? 'PUT' : 'POST',
      headers: myHeaders,
      body: JSON.stringify(person),
      mode: 'cors',
    };
    let url = `${this.host}/${this.endPoints.persons}`;
    if (person._id) {
      url = `${url}/${person._id}`;
    }
    return await fetch(
      url,
      config
    ).then((response) => response.json());
  }

  async deletePerson(id) {
    let config = {
      method: 'DELETE',
      mode: 'cors',
    };
    let url = `${this.host}/${this.endPoints.persons}/${id}`;
    return await fetch(
      url,
      config
    ).then((response) => response.text());
  }
  
}
