export class BaseService {
  constructor() {
    this.host = window.AppConfig.services.host;
    this.endPoints = window.AppConfig.services.endPoints;
  }
}
