import { css } from 'lit-element';

export default css`

    .main-page {
        background-color: #D3D3D3;
        margin: 0;
        padding: 15px;
        height: 100%;
        width: 100%;
    }

    .main-container {
        margin-top: 45px;
        padding: 15px;
        background-color: #fff;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
        width: calc(100% - 60px);
        height: calc(100vh - 105px)
    }

    .table-person {
        --bg-header-table: #072146;
    }

    .container-form-person {
        margin: 20px auto 10px auto;
        width: 450px;
    }

    .container-form-person h2 {
        border-bottom: 1px solid #666;
        padding-bottom: 7px;
        text-align: center;
        font-weight: normal;
        margin: 0;
        margin-bottom: 15px;
    }

`;