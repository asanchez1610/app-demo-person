// Import here your LitElement initial components (critical / startup)

import '@bbva-web-components/bbva-core-scoping-ambients-shim';
import '@webcomponents/shadycss/entrypoints/custom-style-interface.js';
import '@bbva-web-components/bbva-foundations-theme/bbva-foundations-theme.js';
import '@cells-components/coronita-icons';
import '@demo-components/cells-menu-demo/cells-menu-demo';
import '@demo-components/cells-mask-loading-demo/cells-mask-loading-demo';
import '@demo-components/cells-form-persona-demo/cells-form-persona-demo';
