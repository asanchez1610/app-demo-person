(function() {
  'use strict';

  window.CellsPolymer.start({
    routes: {
      'person-list': '/',
      'person-create': '/create',
      'person-edit': '/persons/:id'
    }
  });
}());
