import { CellsPage } from '@cells/cells-page';
import { html } from 'lit-element';
import stylesPages from '../../elements/styles/styles-pages.js';
import '@cells-components/cells-template-paper-drawer-panel';
import '@demo-components/cells-list-person/cells-list-person';
import '../../elements/dm/person-dm/person-dm.js';
import '@bbva-web-components/bbva-help-modal';

/* eslint-disable new-cap */
class PersonListPage extends CellsPage {
  static get is() {
    return 'person-list-page';
  }

  static get properties() {
    return { personSelected: Object };
  }

  static get styles() {
    return [ stylesPages ];
  }

  firstUpdated() {
    this.loadPersons();
    this.subscribe('ch-load-list-person', () => this.loadPersons());
  }

  onEdit({detail}) {
    console.log('onEdit', detail);
    this.navigate('person-edit', { id: detail._id });
    this.publish('ch-edit-person', detail);
  }

  onDelete({detail}) {
    console.log('onDelete', detail);
    this.personSelected = detail;
    this.shadowRoot.querySelector('#confirmModal').open();
  }

  async deletePerson() {
    console.log('Selected', this.personSelected);
    await this.shadowRoot.querySelector('person-dm').deletePerson(this.personSelected._id);
    this.loadPersons();
    this.personSelected = null;
  }

  render() {
    return html`
    <cells-template-paper-drawer-panel mode="seamed">
      <div slot="app__main">
          <div class="main-page" >
            <div class="main-container">
                <cells-list-person 
                class="table-person"
                @on-edit-person="${this.onEdit}"
                @on-delete-person="${this.onDelete}"
                ></cells-list-person>
            </div>
          </div>
          <person-dm></person-dm>
          <bbva-help-modal
            id="confirmModal"
            header-icon="coronita:info"
            header-text="Eliminar persona"
            button-text="Confirmar"
            @help-modal-footer-button-click=${() => this.deletePerson() }>
            <div slot="slot-content">
              <span>Seguro de eliminar a: ${this.personSelected ? this.personSelected.nombres : ''}</span>
            </div>
          </bbva-help-modal>
      </div>
    </cells-template-paper-drawer-panel>`;
  }

  onPageEnter() {
    console.log('onPageEnter');
  }

  async loadPersons() {
    const data = await this.shadowRoot.querySelector('person-dm').findPerson();
    this.shadowRoot.querySelector('cells-list-person').items = data;
  }

}

window.customElements.define(PersonListPage.is, PersonListPage);
