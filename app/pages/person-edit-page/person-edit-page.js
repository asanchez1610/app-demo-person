import { html } from 'lit-element';
import stylesPages from '../../elements/styles/styles-pages.js';
import { PersonCreatePage } from '../person-create-page/person-create-page.js';
import '@cells-components/cells-template-paper-drawer-panel';
import '../../elements/dm/person-dm/person-dm.js';

/* eslint-disable new-cap */
class PersonEditPage extends PersonCreatePage {
  static get is() {
    return 'person-edit-page';
  }

  static get styles() {
    return [ stylesPages ];
  }

  constructor() {
    super();
  }

  render() {
    return html`
    <cells-template-paper-drawer-panel mode="seamed">
      <div slot="app__main">
      <div class="main-page" >
            <div class="main-container">
              ${this.formPersonTpl}
            </div>
          </div>
          <person-dm></person-dm>
      </div>
    </cells-template-paper-drawer-panel>`;
  }

  onPageEnter() {
    this.subscribe('ch-edit-person', person => this.person = person);
  }

  onPageLeave() {
    this.unsubscribe('ch-edit-person');
  }

}

window.customElements.define(PersonEditPage.is, PersonEditPage);
