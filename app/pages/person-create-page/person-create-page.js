import { CellsPage } from '@cells/cells-page';
import { html, css } from 'lit-element';
import stylesPages from '../../elements/styles/styles-pages.js';
import '@cells-components/cells-template-paper-drawer-panel';
import '../../elements/dm/person-dm/person-dm.js';

/* eslint-disable new-cap */
export class PersonCreatePage extends CellsPage {
  static get is() {
    return 'person-create-page';
  }

  static get properties() {
    return { person: Object };
  }

  constructor() {
    super();
    this.person = {};
  }

  static get styles() {
    return [ stylesPages ];
  }

  async onRegister({detail}) {
    console.log('onRegister', detail);
    await this.shadowRoot.querySelector('person-dm').savePerson(detail);
    this.publish('ch-load-list-person');
  }

  currentPageListado() {
    window.dispatchEvent(
      new CustomEvent('set-current-active-page', {
        detail: 'listPerson',
        bubbles: true,
        composed: true,
      })
    );
  }

  onCancel() {
    this.navigate('person-list');
    this.currentPageListado();
  }

  get formPersonTpl() {
    return html`
          <div class="container-form-person" >
                <h2>${this.person._id ? 'Editar persona' : 'Formulario de registro'}</h2>
                <cells-form-persona-demo
                .person="${this.person}"
                @on-register-person="${this.onRegister}"
                @on-cancel-form = "${this.onCancel}"
                ></cells-form-persona-demo>
          </div>
    `;
  }

  render() {
    return html`
    <cells-template-paper-drawer-panel mode="seamed">
      <div slot="app__main">
      <div class="main-page" >
            <div class="main-container">
              ${this.formPersonTpl}
            </div>
          </div>
          <person-dm></person-dm>
      </div>
    </cells-template-paper-drawer-panel>`;
  }

  onPageEnter() {
    // Cada vez que accedamos a la pagina.
  }

  onPageLeave() {
    // Cada vez que salgamos de la pagina.
  }

}

window.customElements.define(PersonCreatePage.is, PersonCreatePage);
